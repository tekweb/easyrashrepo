Questa cartella conterrà i file relativi alla grafica del sito.

img   -> Le immagini
css   -> I fogli di stile
fonts -> I font personalizzati e le glyphicon di bootstrap.
js    -> File javascript scritti per l’app e non per Angular.
lib   -> Librerie di terzi es. JQuery, Bootstrap, … 