// create the controller and inject Angular's $scope
app.controller('HomeController', function($scope) {
    // create a message to display in our view
    $scope.message = 'HomePage!';
});
