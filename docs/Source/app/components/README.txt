Questa cartella conterrà le componenti delle pagine principali dell’applicazione.
Ogni componente viene trattata come una mini Angular app

Struttura esempio:
|____HomePage.html
|____HomePageController.js
|____HomePageService.js
