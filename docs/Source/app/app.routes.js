app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
	.when('/', {
    	templateUrl: 'app/components/home/home.html',
    	controller: 'HomeController'
  	})

  	.when('/login', {
  		templateUrl: 'app/components/login/login.html',
  		controller: 'LoginController'
  	});
}]);