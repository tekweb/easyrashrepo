var exports = module.exports = {};

exports.getUserType = function(submission,id) {

    var type = 'chair';

    submission.reviewers.forEach(function(reviewer){

        if(reviewer == id) type = 'reviewer';
    });

    submission.authors.forEach(function(reviewer){

        if(reviewer == id) type = 'author';
    });

    return type;
};
