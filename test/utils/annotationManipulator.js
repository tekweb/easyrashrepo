var KEYS = require('../keys.js'),
  cheerio = require('cheerio'),
  exports = module.exports = {},
  xsltproc = require('xsltproc'),
  AdmZip = require('adm-zip'),
  fs = require('fs-extra'),
  epubZip = require('epub-zip');

exports.createHeader = function(type, author, pos, date, uri) {
  var id = '#' + type + pos;
  if (type == 'review') {
    return {
      '@context': KEYS.context,
      '@type': type,
      '@id': id,
      'article': {
        '@id': uri,
        'eval': {
          '@id': id + '-eval',
          '@type': KEYS.score,
          'status': KEYS.state.under_review,
          'rate': {
            '@type': 'xsd:integer',
            'value': ''
          },
          'text': '',
          'author': author,
          'date': date
        }
      },
      'comments': ['#' + type + pos + '-c1']
    };
  } else {
    return {
      '@context': KEYS.context,
      '@type': type,
      '@id': id,
      'article': {
        '@id': uri,
        'eval': {
          '@id': id + '-eval',
          '@type': KEYS.score,
          'status': KEYS.state.under_review,
          'text': '',
          'author': author,
          'date': date
        }
      },
      'comments': ['#' + type + pos + '-c1']
    };
  }
};

exports.createHeaderWithoutComments = function(type, author, pos, date, uri, status, rate, text) {

  var id = '#' + type + pos;
  if (type == 'review') {
    return {
      '@context': KEYS.context,
      '@type': type,
      '@id': id,
      'article': {
        '@id': uri,
        'eval': {
          '@id': id + '-eval',
          '@type': KEYS.score,
          'status': status,
          'rate': {
            '@type': 'xsd:integer',
            'value': rate
          },
          'text': text,
          'author': author,
          'date': date
        }
      },
      'comments': []
    };
  } else {
    return {
      '@context': KEYS.context,
      '@type': type,
      '@id': id,
      'article': {
        '@id': uri,
        'eval': {
          '@id': id + '-eval',
          '@type': KEYS.score,
          'status': status,
          'text': text,
          'author': author,
          'date': date
        }
      },
      'comments': []
    };
  }
};

exports.crateComment = function(text, author, ref, date, id, fullname, user) {

  return {
    '@context': KEYS.context,
    '@type': KEYS.comment,
    '@id': id,
    'text': text,
    'ref': ref,
    'author': author,
    'user': user,
    'fullname': fullname,
    'date': date
  };
};

exports.createPerson = function(type, r_position, user_id, name, uri) {

  var id = '#role' + r_position;

  return {
    '@context': KEYS.context,
    '@type': KEYS.person,
    '@id': user_id,
    'name': name,
    'as': {
      '@id': id,
      '@type': KEYS.role,
      'role_type': 'pro:' + type,
      'in': uri
    }
  };
};

exports.getParsedRash = function(rashContent, submission, user) {

  $ = cheerio.load(rashContent);

  var result = {
    authors: []
  };

  //prendo il corpo del documento
  result.body = $('body').html();

  //prendo il titolo del documento
  //result.title = $('title[property="dcterms:title"]').html();
  result.title = $('title').html();

  //prendo tutti i nomi degli autori del documento
  $('meta[about^="mailto:"][name="dc.creator"]').each(function() {

    var about = $(this).attr('about');

    var affiliations = [];

    var author = {
      'name': $('meta[about="' + about + '"][name="dc.creator"]').attr('content'),
      'email': $('meta[about="' + about + '"][property="schema:email"]').attr('content')
    };

    $('link[about="' + about + '"][property="schema:affiliation"]').each(function() {

      var link = $(this).attr('href');
      affiliations.push($('meta[about="' + link + '"]').attr('content'));
    });

    if (affiliations.length > 0) author.affiliations = affiliations;

    result.authors.push(author);
  });

  //prendo tutti gli script in formato json+ld
  var tmpAnnotations = [];
  $('script[type="application/ld+json"]').each(function() {

    tmpAnnotations.push(JSON.parse($(this).html()));
  });

  //prend tutte le keywords
  var tmpKeywords = [];
  $('meta[property="prism:keyword"]').each(function() {

    tmpKeywords.push($(this).attr("content"));
  });

  var tmpCategories = [];
  $('meta[name="dcterms.subject"]').each(function() {

    tmpCategories.push($(this).attr('content'));
  });

  if (tmpCategories.length > 0)
    result.categories = tmpCategories;

  if (tmpAnnotations.length > 0)
    result.annotations = tmpAnnotations;

  if (tmpKeywords.length > 0)
    result.keywords = tmpKeywords;

  result.state = getArticleStateSync(rashContent);

  return result;
};

function getArticleStateSync(articleAsString) {

  $ = cheerio.load(articleAsString);

  var scriptHead, state = KEYS.state.under_review,
    count = $(KEYS.jsonScript).get().length;

  for (var i = 0; i < count; i++) {

    scriptHead = JSON.parse($(KEYS.jsonScript).eq(i).html())[0];

    console.log(scriptHead['@type']);
    console.log(KEYS.type.decision);

    if (scriptHead['@type'] === KEYS.type.decision) {

      return scriptHead.article.eval.status;
    }
  }
  return state;
}

exports.generateEpubFromRash = function(article, callback) {

  var root = __dirname + '/../',
    inputRashPath = root + 'data/articles/' + article,
    inputRashName = article.replace('.html', ''),
    outputRashPath = root + 'data/output/' + inputRashName,
    outputRashName = outputRashPath + '.epub';

  //Creo tutte le cartelle necessarie se non esistono
  if (!fs.existsSync(inputRashPath)) {

    fs.mkdirSync(inputRashPath);

    fs.mkdirSync(outputRashPath + '/img');
    fs.mkdirSync(outputRashPath + '/OEBPS');
    fs.mkdirSync(outputRashPath + '/META-INF');
  }

  var readedFile = fs.readFileSync(inputRashPath, 'utf8');

  $ = cheerio.load(readedFile);

  var imgsCount = $('figure').length,
    id, src;

  //if(imgsCount>0 && !fs.existsSync(outputRashPath+'/img/'))


  for (var i = 0; i < imgsCount; i++) {

    src = $('figure').eq(i).find('p img').attr('src');

    if (src)
      fs.copySync(KEYS.folders.assets + '/' + src, outputRashPath + '/OEBPS/' + src);

  }

  //metto css bootstrap
  fs.copySync(KEYS.folders.assets + '/css/bootstrap.min.css', outputRashPath + '/OEBPS/bootstrap.min.css');
  fs.copySync(KEYS.folders.assets + '/css/epub_rash.css', outputRashPath + '/OEBPS/rash.css');

  //Inizio la trasformazione
  xslt = xsltproc.transform('to-epub.xsl', inputRashPath, {
    output: outputRashPath + '/META-INF/container.xml',
    stringparam: {
      key: 'uri',
      val: inputRashName
    }
  });

  //trasformazione conclusa
  xslt.on('exit', function(code) {

    if (code === 0) {

      var content = epubZip(outputRashPath);
      fs.writeFileSync(outputRashPath + '.epub', content);
      callback(null, inputRashName + '.epub');
    } else {

      var message = '';
      switch (code) {

        case 1:
          message = 'no argument';
          break;

        case 2:
          message = 'too many parameters';
          break;

        case 3:
          message = 'unknown option';
          break;

        case 4:
          message = 'failed to parse the stylesheet';
          break;

        case 5:
          message = 'error in the stylesheet';
          break;

        case 6:
          message = 'error in one of the documents';
          break;

        case 7:
          message = 'unsupported xsl:output method';
          break;

        case 8:
          message = 'string parameter contains both quote and double-quotes';
          break;

        case 9:
          message = 'internal processing error';
          break;

        case 10:
          message = 'processing was stopped by a terminating message';
          break;

        case 11:
          message = 'could not write the result to the output file';
          break;
      }

      callback({
        'code': code,
        'message': message
      });
    }
  });
};
