var xsltproc = require('xsltproc'),
    dir = '../data/epub/';

var exports = module.exports = {};

exports.convertEpub = function(){

    xslt = xsltproc.transform(dir+'input/stylesheet.xsl', dir+'input/dataset.xml', {
      "profile": true,
      "output": dir+"output/output.html"
    });

    xslt.on('exit', function (code) {
      return 'xsltproc process exited with code ' + code;
    });
};
