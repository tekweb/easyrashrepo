<?xml version="1.0"?>
<xsl:stylesheet
version="1.0"
xmlns="http://www.w3.org/1999/xhtml"
xmlns:x="http://www.w3.org/1999/xhtml"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:opf="http://www.idpf.org/2007/opf"
xmlns:exsl="http://exslt.org/common"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:date="http://exslt.org/dates-and-times"
extension-element-prefixes="exsl date"
exclude-result-prefixes="x opf dc">

<!--
From RASH to EPUB XSLT transformation file - Version 0.0.1

authors: @Spino9330, @Luce_Bottazzo

@important:

    XSLTPROC response codes
    0: normal
    1: no argument
    2: too many parameters
    3: unknown option
    4: failed to parse the stylesheet
    5: error in the stylesheet
    6: error in one of the documents
    7: unsupported xsl:output method
    8: string parameter contains both quote and double-quotes
    9: internal processing error
    10: processing was stopped by a terminating message

-->
  <xsl:variable name="lang" select="'en-US'" />
  <xsl:variable name="doc-bibliography" select="count(//x:section[@role='doc-bibliography']/preceding-sibling::x:section)+1"/>
  <xsl:variable name="doc-footnotes" select="count(//x:section[@role='doc-footnotes']/preceding-sibling::x:section)+1"/>
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:strip-space elements="*" />

  <!-- root document -->
  <xsl:template match="/">

    <container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
      <rootfiles>
        <rootfile full-path="OEBPS/content.opf" media-type="application/oebps-package+xml" />
      </rootfiles>
    </container>

    <!-- output content.opf -->
    <exsl:document href="data/output/{$uri}/OEBPS/content.opf" method="xml" indent="yes">

      <!-- root package elemnt inside content -->
      <package version="3.0" unique-identifier="dcidid"
          xmlns="http://www.idpf.org/2007/opf"
          xmlns:opf="http://www.idpf.org/2007/opf"
          xmlns:dcterms="http://dublincore.org/documents/dcmi-terms/"
          xmlns:dc="http://purl.org/dc/elements/1.1/">

        <!-- dc required elements -->
        <metadata>
          <dc:identifier id="dcidid">http://<xsl:value-of select="$uri"/>.html</dc:identifier>

          <!-- title -->
          <dc:title>
            <xsl:variable name="title" select="//x:title"/>
            <xsl:choose>
              <xsl:when test="contains($title,'--')">
                <xsl:value-of select="substring-before(string(//x:title), '--')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$title"/>
              </xsl:otherwise>
            </xsl:choose>
          </dc:title>

          <!-- lang -->
          <dc:language><xsl:value-of select="$lang"/></dc:language>

          <!-- creators -->
          <xsl:for-each select="//x:meta[@name='dc.creator']">
            <dc:creator>
              <xsl:value-of select="@content"/>
            </dc:creator>
          </xsl:for-each>

          <!-- modified -->
          <meta property="dcterms:modified">2011-01-01T12:00:00Z</meta>
        </metadata>

        <manifest>

          <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml" />

          <item id="htmltoc" properties="nav" media-type="application/xhtml+xml" href="nav.xhtml"/>

          <item id="rash" media-type="text/css" href="rash.css" />

          <item id="bootstrap.min" media-type="text/css" href="bootstrap.min.css" />

          <!-- bind all section element -->
          <xsl:for-each select="x:html/x:body/x:section[x:h1 or @role]">
            <item id="s{position()}" href="section{position()}.xhtml" media-type="application/xhtml+xml" />
          </xsl:for-each>

          <xsl:for-each select="//x:img">
            <item id="{../../@id}" href="{@src}" media-type="image/png" />
          </xsl:for-each>

        </manifest>

            <!-- ordered list of sections -->
        <spine toc="ncx">
          <itemref idref="htmltoc" />
          <xsl:for-each select="x:html/x:body/x:section[./x:h1 or @role='doc-footnotes']">
            <itemref idref="s{position()}"/>
          </xsl:for-each>
        </spine>

      </package>

    </exsl:document>

    <!-- output toc.ncx -->
    <exsl:document href="data/output/{$uri}/OEBPS/toc.ncx" method="xml" indent="yes">

      <!-- ncx tag with elements -->
      <ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1" xml:lang="en">

        <head><meta name="dtb:uid" content="{$uri}.html"/></head>

        <docTitle>
          <!-- get only main title -->
          <xsl:variable name="title" select="//x:title"/>

          <xsl:choose>
            <xsl:when test="contains($title,'--')">
              <text><xsl:value-of select="substring-before(string(//x:title), '--')"/></text>
            </xsl:when>
            <xsl:otherwise>
              <text><xsl:value-of select="$title"/></text>
            </xsl:otherwise>
            </xsl:choose>

          </docTitle>

        <!-- list of creator without DC -->
        <docAuthor>
          <text><xsl:value-of select="//x:meta[@name='dc.creator' and 1]/@content"/></text>
        </docAuthor>

        <!-- navigation map with index -->
        <navMap>

          <!-- select all section with h1 child -->
          <xsl:for-each select="x:html/x:body/x:section[./x:h1 or @role]">

            <navPoint class="h1" id="s{position()}">
              <navLabel>
                <xsl:choose>
                  <xsl:when test="@role">
                    <xsl:if test="@role='doc-abstract'"><text>Abstract</text></xsl:if>
                    <xsl:if test="@role='doc-acknowledgements'"><text>Acknowledgements</text></xsl:if>
                    <xsl:if test="@role='doc-bibliography'"><text>Bibliography</text></xsl:if>
                    <xsl:if test="@role='doc-footnotes'"><text>Footnotes</text></xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <text><xsl:value-of select="1+count(preceding-sibling::x:section[not(@role)])"/>. <xsl:value-of select="x:h1"/></text>
                  </xsl:otherwise>
                </xsl:choose>
              </navLabel>
              <content src="section{position()}.xhtml"/>
            </navPoint>
          </xsl:for-each>
        </navMap>

        <pageList>

        <xsl:for-each select="x:html/x:body/x:section">

          <pageTarget id="section{position()}" type="normal" value="{position()}">
            <navLabel>
              <xsl:choose>
                <xsl:when test="@role">
                  <xsl:if test="@role='doc-abstract'"><text>Abstract</text></xsl:if>
                  <xsl:if test="@role='doc-acknowledgements'"><text>Acknowledgements</text></xsl:if>
                  <xsl:if test="@role='doc-bibliography'"><text>Bibliography</text></xsl:if>
                  <xsl:if test="@role='doc-footnotes'"><text>Footnotes</text></xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <text><xsl:value-of select="1+count(preceding-sibling::x:section[not(@role)])"/>. <xsl:value-of select="x:h1"/></text>
                </xsl:otherwise>
              </xsl:choose>
            </navLabel>
            <content src="section{position()}.xhtml"/>
          </pageTarget>
        </xsl:for-each>
        </pageList>

      </ncx>
    </exsl:document>

    <!-- create nav element -->
    <exsl:document href="data/output/{$uri}/OEBPS/nav.xhtml" method="xml" indent="yes">

      <html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
        <head><title>Table of contents</title></head>
        <body>
          <section epub:type="landmarks" hidden="hidden">
            <h1>Table of Contents</h1>
            <nav epub:type="toc" id="toc">
              <ol>
                <xsl:for-each select="x:html/x:body/x:section">

                  <xsl:variable name="i" select="position()"/>

                  <li id="s{$i}">

                    <xsl:variable name="thisPosition" select="1+count(preceding-sibling::x:section[not(@role)])" />

                    <xsl:choose>
                      <xsl:when test="@role">
                        <xsl:if test="@role='doc-abstract'"><a href="section{position()}.xhtml">Abstract</a></xsl:if>
                        <xsl:if test="@role='doc-acknowledgements'"><a href="section{position()}.xhtml">Acknowledgements</a></xsl:if>
                        <xsl:if test="@role='doc-bibliography'"><a href="section{position()}.xhtml">Bibliography</a></xsl:if>
                        <xsl:if test="@role='doc-footnotes'"><a href="section{position()}.xhtml">Footnotes</a></xsl:if>
                      </xsl:when>
                      <xsl:otherwise>
                        <a href="section{$i}.xhtml"><xsl:value-of select="$thisPosition"/>. <xsl:value-of select="x:h1"/></a>
                      </xsl:otherwise>
                    </xsl:choose>

                    <!-- check if has sub section not a doc-footnote -->
                    <xsl:if test="x:section[not(@role='doc-footnote')]">
                      <ol>
                        <xsl:for-each select="x:section">

                          <xsl:variable name="j" select="position()" />

                          <li id="s{$i}.{$j}">
                            <xsl:choose>
                              <xsl:when test="@id">
                                <a href="section{$i}.xhtml#{@id}"><xsl:value-of select="$thisPosition"/>.<xsl:value-of select="$j"/>. <xsl:value-of select="@id"/></a>
                              </xsl:when>
                              <xsl:otherwise>
                                <a href="section{$i}.xhtml#s{1+count(preceding-sibling::x:section)}"><xsl:value-of select="$thisPosition"/>.<xsl:value-of select="$j"/>. <xsl:value-of select="x:h1"/></a>
                              </xsl:otherwise>
                            </xsl:choose>
                          </li>
                        </xsl:for-each>
                      </ol>
                    </xsl:if>
                  </li>
                </xsl:for-each>
              </ol>
            </nav>
          </section>
        </body>
      </html>
    </exsl:document>

      <!-- create section file for each section -->

    <xsl:for-each select="x:html/x:body/x:section[./x:h1 or @role]">

      <xsl:variable name="i" select="position()"/>
      <!-- create all section files -->
      <exsl:document href="data/output/{$uri}/OEBPS/section{$i}.xhtml" method="xml" indent="yes">

        <html xmlns:iml="http://www.w3.org/1999/xhtml">
          <head>
            <title>
              <xsl:choose>
                <xsl:when test="@role">
                  <xsl:if test="@role='doc-abstract'">Abstract</xsl:if>
                  <xsl:if test="@role='doc-acknowledgements'">Acknoledgments</xsl:if>
                  <xsl:if test="@role='doc-bibliography'">Bibliography</xsl:if>
                  <xsl:if test="@role='doc-footnotes'">Footnotes</xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="1+count(preceding-sibling::x:section[not(@role)])"/>. <xsl:value-of select="x:h1"/>


                </xsl:otherwise>
              </xsl:choose>
            </title>
            <link rel="stylesheet" href="bootstrap.min.css" type="text/css"/>
            <link rel="stylesheet" href="rash.css" type="text/css"/>
          </head>
          <body>
            <xsl:copy>
              <!-- set h1 with position eg. 1. section1 -->
              <xsl:choose>
                <xsl:when test="@role">
                  <xsl:if test="@role='doc-abstract'"><h1>Abstract</h1><xsl:apply-templates /></xsl:if>
                  <xsl:if test="@role='doc-acknowledgements'"><h1>Acknowledgments</h1><xsl:apply-templates /></xsl:if>
                  <xsl:if test="@role='doc-bibliography'"><h1>Bibliography</h1><xsl:apply-templates /></xsl:if>
                  <xsl:if test="@role='doc-footnotes'"><h1>Footnotes</h1><ol><xsl:apply-templates /></ol></xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <h1><xsl:value-of select="1+count(preceding-sibling::x:section[not(@role)])"/>. <xsl:value-of select="x:h1"/></h1>
                  <xsl:apply-templates />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:copy>
          </body>
        </html>
      </exsl:document>
    </xsl:for-each>

  </xsl:template>

  <!-- MATCHED TEMPLATES -->

  <!-- create references and set anchor -->
  <xsl:template match="x:section//x:a[not(text())]">

    <xsl:variable name="ref" select="substring-after(string(@href), '#')"/>

    <xsl:choose>
    <!-- case footnote -->
      <xsl:when test="//x:section[@id=$ref and @role='doc-footnote']">
        <!--role="doc-footnote"-->
        <a href="section{$doc-footnotes}.xhtml{@href}"><sup><xsl:value-of select="count(//x:section[@id=$ref]/preceding-sibling::*) + 1"/></sup></a>
      </xsl:when>
      <!-- case section -->
      <xsl:when test="//x:section[@id=$ref and not(@role='doc-footnote')]">
        <xsl:variable name="section" select="count(//x:section[@id=$ref and not(role)]/preceding-sibling::*)+1"/>
        <xsl:variable name="newSec" select="count(//x:section[@id=$ref and not(role)]/preceding-sibling::*)"/>
        <a href="section{$section}.xhtml">section <xsl:value-of select="$newSec"/></a>
      </xsl:when>
      <!-- case li -->
      <xsl:when test="//x:li[@id=$ref]">
        <a href="section{$doc-bibliography}.xhtml{@href}">[<xsl:value-of select="count(//x:li[@id = $ref]/preceding-sibling::*)+1"/>]</a>
      </xsl:when>
      <!-- case table -->
      <xsl:when test="//x:table[../@id=$ref]">
        <xsl:variable name="section" select="count(//x:section[//x:table[../@id=$ref]]/preceding-sibling::x:section)+1"/>
        <xsl:variable name="table" select="count(//x:figure[x:table and @id=$ref]/preceding-sibling::x:figure[x:table]) + 1"/>
        Table <xsl:value-of select="$table"/>
      </xsl:when>
      <!-- case figure -->
      <xsl:when test="//x:img[../../@id=$ref]">
        <xsl:variable name="section" select="count(../../preceding-sibling::*)+1"/>
        <xsl:variable name="img" select="count(//x:figure[//x:img and @href=$ref]/preceding-sibling::x:figure[//x:img]) + 1"/>
        Figure <xsl:value-of select="$img"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- recursive copy of all section children -->
  <xsl:template match="x:section//@*|x:section//node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="x:section//x:section[not(@id)]">
    <section id="s{count(preceding-sibling::x:section)+1}">
      <xsl:apply-templates/>
    </section>
  </xsl:template>

  <!-- remove H1 elements -->
  <xsl:template match="x:body/x:section/x:h1"/>

  <!-- enumerate all subsections -->
  <xsl:template match="x:body/x:section/x:section/x:h1">
    <xsl:variable name="sectionPosition" select="count(../../preceding-sibling::x:section[not(@role)]) +1"/>
    <xsl:variable name="subSectionPosition" select="count(../preceding-sibling::x:section) +1"/>
    <h2><xsl:value-of select="$sectionPosition"/>.<xsl:value-of select="$subSectionPosition"/>.  <xsl:value-of select="."/></h2>
    <xsl:apply-templates/>
  </xsl:template>

  <!-- enumerate all subsection of subsection -->
  <xsl:template match="x:body/x:section/x:section/x:section/x:h1">
    <xsl:variable name="sectionPosition" select="count(../../../preceding-sibling::x:section[not(@role)]) +1"/>
    <xsl:variable name="subSectionPosition" select="count(../../preceding-sibling::x:section) +1"/>
    <xsl:variable name="subSubSectionPosition" select="count(../preceding-sibling::x:section) +1"/>
    <h3><xsl:value-of select="$sectionPosition"/>.<xsl:value-of select="$subSectionPosition"/>.<xsl:value-of select="$subSubSectionPosition"/>.  <xsl:value-of select="."/></h3>
    <xsl:apply-templates/>
  </xsl:template>

  <!-- transform unordered list into ordered -->
  <xsl:template match="x:section[@role='doc-bibliography']//x:ul" >
    <ol><xsl:apply-templates /></ol>
  </xsl:template>

  <xsl:template match="x:section[@role='doc-footnote']">
    <li id="{@id}" role="{@role}"><xsl:apply-templates /></li>
  </xsl:template>

</xsl:stylesheet>
