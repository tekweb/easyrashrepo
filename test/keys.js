module.exports = {

    //JSON+LD
    context: 'http://vitali.web.cs.unibo.it/twiki/pub/TechWeb16/context.json',
    jsonScript: 'script[type="application/ld+json"]',
    state:{

        under_review: 'pso:under_review',
        accepted: 'pso:accepted_for_publication',
        rejected: 'pso:rejected_for_publication'
    },
    type:{
        comment: 'comment',
        decision: 'decision'
    },
    score: 'score',
    person: 'person',
    role: 'role',

    //database tables
    conferences: 'events',

    folders:{
        assets:__dirname+'/public/assets'
    }
};
