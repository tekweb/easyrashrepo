var app = angular.module('app', ['ngRoute', 'angularModalService', 'angularMoment']);
//app.constant('url', 'http://annina.cs.unibo.it:8080');
app.constant('url', 'http://localhost:3000');

app.run(function($rootScope) {
  // Aggiungi il nostro token a tutte le richieste
  $.ajaxPrefilter(function(options) {
    if (!options.beforeSend) {
      options.beforeSend = function(xhr) {
        xhr.setRequestHeader('x-access-token', window.sessionStorage.token);
      }
    }
  });
});
