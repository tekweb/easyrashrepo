/* Custom Directives */
app.directive('annotationView', function() {
    return {
    	scope: false,
    
    	template: '<div ng-bind-html="articleView"></div>',

        link: function($scope, element, attrs) {


        	$scope.$watch('articleView', function(newValues, oldValues, scope) {
        		if($scope.articleView){
                    $scope.showModal = false;
                    $scope.initMyPlugin();
        			$scope.initAnnotator($scope.articleView.role , $scope.username);	
                    //initRashDocument();
        			$scope.annotator.flag = true;
        			initRashDoc();
        		}
        	});

            $scope.initMyPlugin = function ( ){
                Annotator.Plugin.MyPlugin = function (element) {
                    return {
                        pluginInit: function () {
                            this.annotator
                            .subscribe("annotationCreated", function (annotation) {
                                $scope.getArticle($scope.articleView.fulltitle, $scope.articleUrl);
                                bootbox.alert("Annotazione creata con successo.")
                            })
                            .subscribe("annotationUpdated", function (annotation) {
                                bootbox.alert("Annotazione modificata correttamente.");
                            })
                            .subscribe("annotationDeleted", function(annotation){
                                $scope.getArticle($scope.articleView.fulltitle, $scope.articleUrl);
                                bootbox.alert("Annotazione eliminata");
                            })
                            .subscribe("annotationEditorShown", function(editor, annotation){
                                $(".annotator-checkbox").hide();
                            })
                        }
                    }
                }
            }

        	$scope.initAnnotator= function (role, email){
        		delete $scope.content;
    			var readable = (role == 'pc_member') || (role == 'chair') || $scope.articleView.madeMyReview;

    			$scope.content = $('#content').annotator({
					readOnly: readable
				});

                //Implementa listeners per alcuni eventi di AnnotatorJS
                $scope.content.annotator("addPlugin", "MyPlugin");

                $scope.content.annotator('addPlugin', 'Permissions', {
                    user: email,
                    permissions: {
                        'read':   [],
                        'update': [email],
                        'delete': [email],
                        'admin':  [email]
                    }
                });
				//aggiungo i plugin all'annotator
				$scope.content.annotator('addPlugin', 'Store', {

					// The endpoint of the store on your server.
					prefix: '/api',

					// Attach the uri of the current page to all annotations to allow search.
					annotationData: {
						'uri': $scope.articleUrl,
                        'user': email,
                        'author': email,
                        'fullname' : $scope.fullname,
                        'role': $scope.articleView.role
                        //'tags' : [ $scope.email ]
					},

                    urls: {
                        // These are the default URLs.
                        create:  '/annotations',
                        update:  '/annotations/:id',
                        destroy: '/annotations/:id',
                        search:  '/search' // ANNOTATIONS GET
                      },

					// This will perform a "search" action when the plugin loads. Will
					// request the last 20 annotations for the current url.
					// eg. /store/endpoint/search?limit=20&uri=http://this/document/only
					loadFromSearch: {
						'limit': 20,
						'uri': $scope.articleUrl
					}
				});

                //Aggiungo il plugin di ricerca per autore
                $scope.content.annotator('addPlugin', 'Filter', {
                    filters: [
                    {
                        label: 'Author',
                      	property: 'fullname',
                      	isFiltered: function (input, fullname) {
                            if (input && fullname && fullname.length) {
                              	input = input.toUpperCase();
                              	fullname = fullname.toUpperCase();
                              	if(fullname.indexOf(input) !== -1){
                              		return true;
                              	}
                            }
                            return false;
                        }
                    }]  
                });

                //Plugin per compatibilità di annotator con dispositivi mobile
                $scope.content.annotator("addPlugin", "Touch");
        	}

            $scope.refreshContent = function (article) {
                $('#content').remove();
                $('.annotator-filter').remove();
                $("html").attr("style", "padding-top: 33px");
                $('#content_parent').append("<div annotation-view id='content'>"+article+"</div>");
                $('p').addClass("text-justify");
                //addBootstrapClasses();
                resizeImages();
                $(window).resize(function(){
                    $scope.$apply(function(){
                    	resizeImages();
                    });
                });
            }

            $scope.logoutAnnotator = function (){
            	$('#content').remove();
            	$('.annotator-filter').remove();
            	$("html").attr("style", "padding-top: 0px");
            }

            $scope.showAnnotations = function () {
            	if($scope.annotator.flag){
            		$('span.annotator-hl').addClass('hideAnnotation');
            		$('span.annotator-hl').removeClass('annotator-hl');
            	} else {
            		$('span.hideAnnotation').addClass('annotator-hl');
            		$('span.annotator-hl').removeClass('hideAnnotation');
            	}
            	$scope.annotator.flag = !$scope.annotator.flag;
            }

            $scope.scrollToPointer = function (current_id) {
                console.log("Scrolling");
                var str = 'fn_pointer_'+current_id;
                $location.hash(str);
            }
            
            function initRashDoc(){

            	var script = document.createElement( 'script' );
            	script.type = 'text/javascript';
            	script.src = 'assets/js/rash.js';
            	$("#content").prepend( script );

                $("a").on('click', function(e){
                    var s = $(this).attr('href');
                    if(s.startsWith('#')){
                        e.preventDefault();
                        var elem = $(s);
                        $("html, body").animate({ scrollTop: elem.offset().top - 100 }, 1000);

                        elem.css("-webkit-transition", "all 2s");
                        elem.css("-moz-transition", "all 2s");
                        elem.css("-o-transition", "all 2s");
                        elem.css("transition", "all 2s");

                        elem.addClass("highlightAns");
                		// Remove that class after 2 seconds                       
                    	setTimeout(function(){
                        	elem.removeClass("highlightAns");
                    	},2000);
                    }
                });


            }

            function resizeImages() {
            	$('#content img').width($('#content').width());
            }

		}
    }
});

/* Directive per la visualizzazione dei Metadata di un articolo*/
app.directive('articleMeta', function(){
    return {
        scope: false,
        templateUrl: 'components/article/articleMetaData.html'
    }
});


app.directive('annotationsList', function (){
	return {
        scope: false,
		templateUrl: 'components/home/annotationsList.html'
	}
});

app.directive('ratingBar', function (){
    return {
        scope: false,

        templateUrl: 'components/home/BarRating.html',
        
        link: function($scope, element, attrs){

            $scope.$watch('rating', function(newValues, oldValues, scope) {
                init();
            });

            function init() {
                $('#rateselect').barrating({
                    theme: 'bars-square',
                    fastClicks: true,
                    initialRating: $scope.rating,
                    showValues: true,
                    showSelectedRating: false,
                    readonly: false,
                    onSelect: function(value, text, event) {
                        if (typeof(event) !== 'undefined') {
                            $scope.rating = value;
                        }
                    }
                });
            }   
        }
       
    }
});

app.directive('carousel', function(){
    return {
        scope: false,

        templateUrl: 'components/login/carousel.html',
        
        link: function($scope, element, attrs){
            $('.carousel').carousel();    
            console.log("carousel");
        }
    }
});

app.directive('reviewsSummary', function(){
    return {
        scope: false,
        templateUrl: 'components/home/reviewsSummary.html'
    }
})
