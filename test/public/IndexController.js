app.controller('IndexController',  ['$http', '$location', '$window', '$scope', 'httpFactory', function($http, $location, $window, $scope, httpFactory){

	var init = function () {
		if($window.sessionStorage.token == null ){
			$location.url('/login');
		} else {
			$location.url('/home');
		}
	};
	
	init();
}]);