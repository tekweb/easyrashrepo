app.config(['$routeProvider', '$locationProvider',  function($routeProvider, $locationProvider) {
	$routeProvider
    .when('/home', {
        title: 'EasyRash - Home',
		templateUrl: 'components/home/home.html',
		controller: 'HomeController'
	})

  	.when('/login', {
        title: 'EasyRash - Login',
  		templateUrl: 'components/login/login.html',
  		controller: 'LoginController'
  	})
    .otherwise({
        redirectTo: '/'
    });

    //$locationProvider.html5Mode(true);


}]).run( function($rootScope, $location, $window) {

    //Aggiorna <title> a seconda della pagina caricata
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        document.title = current.title;
    });

    // register listener to watch route changes
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {

        if ( $window.sessionStorage.token == null ) {
            // no logged user, we should be going to #login
            if ( next.templateUrl == "components/login/login.html" ) {
              // already going to #login, no redirect needed
            } else {
              // not going to #login, we should redirect now
              $location.path( "/login" );
            }
        } else {
            $location.path("/home");
        }
    });
});
