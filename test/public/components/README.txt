Questa cartella conterrà le componenti delle pagine principali dell’applicazione.
Ogni componente viene trattata come una mini Angular app

Struttura esempio:
home
|____home.html
|____HomePageController.js
|____HomePageService.js

login
|____login.html
|____LoginPageController.js