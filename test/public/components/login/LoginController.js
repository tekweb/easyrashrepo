// create the controller and inject Angular's $scope
app.controller('LoginController', ['$scope', 'httpFactory', '$location', '$window' , function($scope, httpFactory, $location, $window ) {
    // create a message to display in our view

    $scope.message = "So easy.";

	$scope.authenticate = function () {

		var email = $scope.email;
		var password = $scope.password;

		if( email && password ){
      		//parametro salvato come json
			var data = JSON.stringify({
                	email: email,
                	password: password
            });

			httpFactory.authenticate( data ).then(
				function onSuccess ( response ) {
					$window.sessionStorage.token = response.data.token;
					$window.sessionStorage.username = email;
					$window.sessionStorage.fullname = response.data.name;
					$window.sessionStorage.welcomeMessage = response.data.message+" "+response.data.name;

				  	$location.url('/home');
				}, 
				function onError ( response ) {
					delete $window.sessionStorage.username;
					delete $window.sessionStorage.token;

					alert(response.data.message);
				}
			)
		}
	};

}]);
