app.controller('HomeController', ['$scope', '$location', '$sce', '$window', 'httpFactory', 'ModalService', function($scope, $location, $sce, $window, httpFactory, ModalService) {


    //Funzione di Logout
    $scope.logout = function () {
		delete $window.sessionStorage.username;
		delete $window.sessionStorage.token;
  		$location.url('/login');
  		$scope.logoutAnnotator();
	};

	//Funzione che ritorna gli eventi dell'utente
	var getEvents = function () {
		httpFactory.getEvents().then(
			function onSucces( response ){
				if(response.data.success == false){
					alert(response.data.message);
				} else {
					response.data.forEach(function( event ){
						$scope.events.push(event);
					});
				}
			},
			function onError( response ){
				alert('Non sono riuscito ad ottenere gli eventi');
				console.log('Response: '+JSON.stringify( response ));
			});
	}

	//Funzione che ritorna un articolo RASH
	$scope.getArticle = function ( articleTitle, url ) {

		httpFactory.getArticle(url).then(
			function onSucces( response ){

				if(response.data.success == false){
					alert(response.data.message);
				} else {
					delete $scope.articleView;

					$scope.articleUrl = url;
					$scope.articleView = $sce.trustAsHtml(response.data.body);
					$scope.articleView.state = response.data.state;//qui lo stato globale
					$scope.articleView.role = response.data.role;
					$scope.articleView.fulltitle = articleTitle;
					$scope.articleView.avgArticle = response.data.averageRate;
					$scope.articleView.reviewsSummary = response.data.reviewsSummary;
					$scope.articleView.isReadyForDecision = response.data.isReadyForDecision;
					$scope.articleView.title = articleTitle.split('--')[0].trim();
					$scope.articleView.subtitle = articleTitle.split('--')[1];
					$scope.articleView.meta = response.data;

					if($scope.articleView.role == 'reviewer'){
						$scope.articleView.madeMyReview = response.data.madeMyReview;
					}

					$scope.articleView.annotations = [];
					angular.forEach(response.data.annotations, function( elem ){
						for (var i = 1; i < elem.length-1; i++) {
							elem[i].author = elem[elem.length-1];
							$scope.articleView.annotations.push(elem[i]);
						}
					});

					$scope.refreshContent($scope.articleView);
				}
			},
			function onError( response ){
				alert("Non sono riuscito ad ottenere l'articolo desiderato.");
				console.log('Response: '+JSON.stringify( response ));
			});
	}

	$scope.convertAsEpub = function (){
		httpFactory.convert( $scope.articleUrl ).success(
		function (data, status, headers) {
	        var filename = $scope.articleUrl.substring(0, $scope.articleUrl.length-5)+'.epub';
	        var contentType = 'arraybuffer';

	        var linkElement = document.createElement('a');
	        try {
	            var blob = new Blob([data], { type: contentType });
	            var url = window.URL.createObjectURL(blob);

	            linkElement.setAttribute('href', url);
	            linkElement.setAttribute("download", filename);

	            var clickEvent = new MouseEvent("click", {
	                "view": window,
	                "bubbles": true,
	                "cancelable": false
	            });
        		linkElement.dispatchEvent(clickEvent);
    		} catch (ex) {
        		console.log(ex);
    		}
		}).error(function (data) {
    		console.log(data);
		});

	}



	//Funzione di inizializzazione
	var init = function () {
		$scope.showModal = false;
		$scope.annotator = {};
		$scope.annotator.flag = true;
		$scope.annotator.text = "Nascondi Annotazioni";

		$scope.events = [];
		$scope.username = $window.sessionStorage.username;
		$scope.userToken = $window.sessionStorage.token;
		$scope.fullname =  $window.sessionStorage.fullname;
		$scope.welcomeMessage = $window.sessionStorage.welcomeMessage;


		getEvents();
	};


	/* Metodi relativi alla finestra modale di Revisione */

	$scope.reviewModal = function() {
		var myInputs = {};
		if($scope.articleView.role == 'chair'){
			myInputs = {
				avgArticle: $scope.articleView.avgArticle,
				articleUrl: $scope.articleUrl,
				userRole: $scope.articleView.role,
				reviewsSummary: $scope.articleView.reviewsSummary
			}
		} else {
			myInputs = {
				avgArticle: null,
				articleUrl: $scope.articleUrl,
				userRole: $scope.articleView.role,
				reviewsSummary: null
			}
		}
		ModalService.showModal({
        	templateUrl: 'reviewPanel.html',
        	controller: "ModalController",
			inputs: myInputs
    	}).then(function(modal) {
    		modal.element.modal({
        	    backdrop: 'static',
        	    keyboard: false
    		});
        	modal.close.then(function(result) {

            	if(result != 'cancel'){
            		console.log(JSON.stringify(result));
            		httpFactory.review(result);
            		if( result.userRole == 'chair'){
            			$scope.articleView.state =
            			 result.accepted ? 'accepted_for_publication' : 'rejected_for_publication';
            		}
            		$scope.getArticle($scope.articleView.fulltitle, $scope.articleUrl);
            		$scope.articleView.isReadyForDecision = false;
            	}
        	});
    	});
    };

	init();
}]);
