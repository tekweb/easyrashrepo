app.controller('ModalController', function($scope, avgArticle, articleUrl, userRole, reviewsSummary, close) {

	$scope.close = function(flag, accepted, comment, judgement) {

		var result = {};
		if(flag){
			result = {
					"uri" : articleUrl,
					"userRole" : userRole,
					"accepted" : accepted,
					"comment" : comment,
					"judgement" : judgement
			};
		} else {
			result = 'cancel'
		}

		close(result, 200); // close, but give 200ms for bootstrap to animate
	};

	var init = function () {
		$scope.avgArticle = avgArticle;
		$scope.accepted = true;
		$scope.userRole = userRole;
		$scope.reviewsSummary = reviewsSummary;
		$scope.rating = 1;
	}
	init();



});