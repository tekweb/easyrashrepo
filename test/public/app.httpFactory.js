app.factory('httpFactory', ['httpRequestInterceptor', '$http', '$window', 'url',  function(httpRequestInterceptor, $http, $window, url){
    
    var httpFactory = {};

    
    /* Post request che ritorna il token di autenticazione o un messaggio di errore */
    httpFactory.authenticate = function ( data ) {
        return $http.post( url + "/api/authenticate/" , data , httpRequestInterceptor);
    }

    httpFactory.getEvents = function () {
         return $http.get( url + "/api/events/" , httpRequestInterceptor);
    }

    httpFactory.getArticle = function ( idArticle ){
        return $http.get ( url + "/api/article/"+idArticle , httpRequestInterceptor);
    }

    httpFactory.review = function ( data ){
        return $http.post( url + "/api/review/", data, httpRequestInterceptor);
    }

    httpFactory.convert = function ( uri ){
        return $http({
                method: 'GET',
                url: url+'/api/convert/'+uri,
                responseType: 'arraybuffer'
            }, httpRequestInterceptor);
    }

    return httpFactory;
}]);


app.factory('httpRequestInterceptor', ['$window' , function ($window) {
    return  {
            request: function (config) {

                config.headers['x-access-token'] = $window.sessionStorage.token;

            return config;
        }
    };
}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
});
