var express = require('express');
var fs = require('fs-extra');
var app = express();
var ms = require('ms');
var colors = require('colors');
var createMutex = require('read-write-lock');
var KEYS = require('./keys.js');

var bodyParser = require('body-parser');
var morgan = require('morgan');

// driver mongo
var MongoClient = require('mongodb').MongoClient;

// per autenticazione e token
var jwt = require('jsonwebtoken');

//file di config con la passphrase e database
var config = require('./config');
var utilities = require('./utils/util');
var manipulator = require('./utils/annotationManipulator');
var epub = require('./utils/epubConverter');

//url settato
var port = process.env.PORT || 3000;
var url = (process.env.URI || config.url) + port;

var db_connection = (process.env.DB || config.database) + (process.env.DB_PORT || config.db_port) + config.db_name;

console.log(db_connection);

var cheerio = require('cheerio');
var mutex = createMutex();

//connessione al database e set della passphrase
//mongoose.connect(config.database);
app.set('superSecret', config.secret);

// setta il body parser per leggere il json delle richieste POST
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

// logger
app.use(morgan('dev'));

MongoClient.connect(db_connection, function(err, db) {

  if (err) {
    console.log(JSON.stringify(err.message).red);
    console.log(JSON.stringify('Eseguire il comando mongod').red);
  }

  DB = db;
});

/*
 *
 * TODO capire il funzionamento di static
 */
app.use(express.static(__dirname + '/public'));

/*
 * Per ogni richiesta get sulla root, ritorna il file index.html
 * che è la base per Angular.js
 */
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

/*
 * apiRoutes è la variabile che fa riferimento al routing
 *
 */
var apiRoutes = express.Router();


/*
 * effettua l'autenticazione dell'utente, con email e password
 * se l'autenticazione è corretta ritorna un token da utilizzare per le api
 */
apiRoutes.post('/authenticate', function(req, res) {

  var user = {
    'email': req.body.email,
    'pass': req.body.password
  };

  DB.collection('users').findOne(user, function(err, doc) {

    if (err) throw err;

    user = doc;

    if (!user) {

      var error = {
        success: false,
        message: 'Autenticazione fallita, email o password non corretta.'
      };
      //errore e forbidden
      console.log(JSON.stringify(error).red);
      return res.status(403).send(error);
    }

    var token = jwt.sign(user, app.get('superSecret'), {
      expiresIn: ms('1w')
    });

    var message = {
      success: true,
      message: 'Welcome back, ',
      name: user.given_name + ' ' + user.family_name,
      token: token
    };
    res.json(message);
  });
});


/*
 * middleware per tutte le successive richieste, contorlla se c'è il token
 * se non c'è, ritorna un errore 403, oppure accede alla risorsa
 */
apiRoutes.use(function(req, res, next) {

  var token = req.headers['x-access-token'];

  // se il token esiste
  if (token) {

    // verifica il token con la chiave segreta
    jwt.verify(token, app.get('superSecret'), function(err, decoded) {

      if (err) {

        var error = {
          success: false,
          message: err.message
        };
        console.log(JSON.stringify(error).red);
        return res.json(error);
      }

      //se va tutto bene, continua con la richiesta
      next();

    });

  } else {

    var error = {
      success: false,
      message: 'Errore, token non trovato'
    };

    // errore se non c'è il token
    console.log(JSON.stringify(error).red);
    return res.status(401).send(error);
  }
});

/*
 * mostra la lista degli utenti nel db
 *
 */
apiRoutes.get('/users', function(req, res) {

  DB.collection('users').find().toArray(function(err, docs) {

    if (err) {

      console.log(JSON.stringify(err).red);
      res.status(500).send(err);
    }

    console.log(('Found : ' + docs.length + ' users').green);
    res.send(docs);
  });
});

apiRoutes.get('/article/:url', function(req, res) {

  //controllo che io posso vedere l'articolo
  //cioè l'utente che chiede l'articolo è reviewer o author dell'articolo
  var user = jwt.decode(req.headers['x-access-token']);
  var url = req.params.url;
  console.log('URL: ' + url);
  var articleUrl = __dirname + '/data/articles/' + url;
  mutex.readLock(function(release) {

    //TODO controllo sull'utente
    fs.readFile(articleUrl, 'utf8', function(err, articleContent) {

      if (err) {

        console.log(JSON.stringify(err).red);
        return res.status(404).send({
          'message': 'Errore, articolo non trovato!'
        });
      }

      //TODO visualizzazioni e permessi
      var cond = {
        'submissions.url': url
      };
      var toFind = {
        "_id": false,
        "submissions.$": true
      };

      //controllo gli articoli
      DB.collection('events').findOne(cond, toFind, function(err, doc) {
        if (err) return res.status(500).send(err);

        var result = manipulator.getParsedRash(articleContent, doc.submissions[0], user);

        getRole(user._id, url, function(err, role) {

          if (err) res.status(500).send(err);
          $ = cheerio.load(articleContent);
          // Se l'articolo è richiesto da un chair, gli indico se almeno 1 reviewer ha espresso
          // la review sull articolo
          if (role == 'chair') {
            var reviewFound = false;
            var isReadyForDecision = false;
            var madeMyDecision = false;
            var totRate = 0;
            var numReview = 0;
            var reviewsSummary = [];
            $('script[type="application/ld+json"]').each(function() {

              var script = JSON.parse($(this).html());
              if (script[0]['@type'] == 'review') {
                var reviewSummary = new Object();
                reviewSummary.email = script[0].article.eval.author.substring(7);
                reviewSummary.decision = script[0].article.eval.status;
                reviewSummary.judgement = script[0].article.eval.rate.value;
                reviewSummary.comment = script[0].article.eval.text;
                reviewsSummary.push(reviewSummary);
              }
              if (script[0].article.eval.status != 'pso:under_review') {
                if (script[0]['@type'] == 'review') {
                  reviewFound = true;
                } else {
                  if (script[script.length - 1]['@id'] == 'mailto:' + user.email) {
                    madeMyDecision = true;
                  }
                }
              }
            });
            result.isReadyForDecision = reviewFound && !madeMyDecision;
            $('script[type="application/ld+json"]').each(function() {
              var script = JSON.parse($(this).html());
              if (script[0].article.eval.status != 'pso:under_review' &&
                script[script.length - 1]['@id'] != 'mailto:' + user.email
              ) {
                totRate += parseFloat(script[0].article.eval.rate.value);
                numReview++;
              }
            });
            var avgRate = parseFloat(parseFloat(totRate) / parseFloat(numReview)).toFixed(2);
            result.averageRate = avgRate;
            result.reviewsSummary = reviewsSummary;
          } else {
            var madeMyReview = false;
            var chairHasClosed = false;
            $('script[type="application/ld+json"]').each(function() {

              var script = JSON.parse($(this).html());
              console.log('script: ' + JSON.stringify(script));
              if (script[0].article.eval.status != 'pso:under_review') {
                if (script[script.length - 1]['@id'] == 'mailto:' + user.email) {
                  madeMyReview = true;
                  console.log('MADEMYREV: ' + madeMyReview);
                } else {
                  if (script[0]['@type'] == 'decision') {
                    chairHasClosed = true;
                    console.log('chairHas: ' + chairHasClosed);
                  }
                }
              }
            });

            result.madeMyReview = madeMyReview;
            result.isReadyForDecision = !madeMyReview && !chairHasClosed;
            console.log(result.isReadyForDecision);
          }
          var state = 'under_review';
          $('script[type="application/ld+json"]').each(function() {
            console.log('entrato');
            var script = JSON.parse($(this).html());
            if (script[0]['@type'] == 'decision') {
              var psoState = script[0].article.eval.status;
              state = psoState.substring(4);
              return false;
            }
          });
          result.state = state;
          console.log('STATE: ' + state);
          result.role = role;
          //console.log('ARTICLE RESPONSE: '+JSON.stringify(result));
          res.send(result);
          release();
        });
      });
    });
  });
});

apiRoutes.get('/search', function(req, res) {

  var articleUrl = __dirname + '/data/articles/' + req.query.uri;

  mutex.readLock(function(release) {

    fs.readFile(articleUrl, function(err, articleContent) {

      if (err) {}

      var result = {
        total: 0,
        rows: []
      };

      $ = cheerio.load(articleContent);

      $('script[type="application/ld+json"]').each(function() {

        var script = JSON.parse($(this).html());

        if (script.length >= 3) {

          for (var i = 1; i < script.length - 1; i++) {

            script[i].ranges = script[i].ref;

            result.rows.push(script[i]);
            result.total++;
          }
        }
      });
      res.send(result);

      release();
    });
  });
});

apiRoutes.post('/annotations', function(req, res) {

  console.log('Post Annot: ' + JSON.stringify(req.body));
  console.log('ROLE: ' + req.body.role);
  var articleUrl = __dirname + '/data/articles/' + req.body.uri;
  var annotationBody = req.body;
  var role = req.body.role;
  var reviewType = role == 'chair' ? 'decision' : 'review';
  mutex.writeLock(function(release) {

    fs.readFile(articleUrl, 'utf8', function(err, articleContent) {

      if (err)
        return res.status(404).send({
          'message': 'Errore, articolo non trovato!'
        });
      var position = -1,
        i = 0,
        user = jwt.decode(req.headers['x-access-token']);
      $ = cheerio.load(articleContent);

      $('script[type="application/ld+json"]').each(function() {

        var script = JSON.parse($(this).html());

        if (script[script.length - 1]['@id'] === 'mailto:' + user.email)
          position = i;

        i++;
      });

      //controllo se esite già uno script dell'utente
      if (position >= 0) {
        //seleziono lo script
        var selectedScript = JSON.parse($('script[type="application/ld+json"]').eq(position).html()),
          commentId;

        if (selectedScript[0].comments.length > 0) {
          var lastId = selectedScript[0].comments[selectedScript[0].comments.length - 1];
          lastId = lastId.split('-c')[1];
          commentId = '#' + reviewType + (position + 1) + '-c' + (parseInt(lastId) + 1);
          console.log('LastID: ' + commentId);
        } else {
          commentId = '#' + reviewType + (position + 1) + '-c1';
        }

        //Pusho il commento nell'array dei comments
        selectedScript[0].comments.push(commentId);
        //aggiungo il commento al penultimo elemento
        selectedScript.splice(selectedScript.length - 1, 0, manipulator.crateComment(
          req.body.text,
          'mailto:' + user.email,
          req.body.ranges,
          new Date(),
          commentId,
          req.body.fullname,
          req.body.user
        ));
        //override dello script
        $('script[type="application/ld+json"]').eq(position).html(JSON.stringify(selectedScript, null, 4) + '\n');
      }

      //se non esiste creo lo script
      else {

        var script = $('<script type="application/ld+json"></script>');
        var tmpScript = [];

        tmpScript[0] = manipulator.createHeader(
          reviewType,
          'mailto:' + user.email,
          i + 1,
          new Date(),
          req.body.uri
        );

        tmpScript[1] = manipulator.crateComment(
          req.body.text,
          'mailto:' + user.email,
          req.body.ranges,
          new Date(),
          '#' + reviewType + (i + 1) + '-c1',
          req.body.fullname,
          req.body.user
        );

        tmpScript[2] = manipulator.createPerson(
          role,
          position,
          'mailto:' + user.email,
          user.given_name + ' ' + user.family_name,
          req.body.uri
        );

        script.html(JSON.stringify(tmpScript, null, 4));
        $('head').append(script + '\n');
      }

      writedArticleContent = $.html();

      fs.writeFile(articleUrl, writedArticleContent, function(err, data) {

        if (err) return res.status(500).send(err);

        res.send({
          'message': 'Commento inserito con successo!'
        });
        release();
      });
    });
  });
});

apiRoutes.get('/convert/:uri', function(req, res) {

  var output = __dirname + '/data/output/';

  manipulator.generateEpubFromRash(req.params.uri, function(err, epub) {

    if (err) {

      console.log(err);
      return res.status(500).send(err);
    }


    res.sendFile(output + epub);
  });
});

/*
 * Eliminazione annotazione
 */
apiRoutes.delete('/annotations', function(req, res) {
  console.log(req.body['@id']);
  console.log(req.body.uri);
  var articleUrl = __dirname + '/data/articles/' + req.body.uri;
  var annotationID = req.body['@id'];
  var split = req.body['@id'].split('-');
  var reviewID = split[0];
  mutex.writeLock(function(release) {
    fs.readFile(articleUrl, 'utf8', function(err, articleContent) {
      $ = cheerio.load(articleContent);
      var i = 0;
      // cerco la review
      $('script[type="application/ld+json"]').each(function() {
        var script = JSON.parse($(this).html());
        if (script[0]['@id'] === reviewID) {
          position = i;
          // cerco il commento
          for (var j = 1; j < script.length; j++) {
            if (script[j]['@id'] == annotationID) {
              script.splice(j, 1);
              //override dello script
              //elimino l'id dall array dei commenti
              for (var k = 0; k < script[0].comments.length; k++) {
                if (script[0].comments[k] == annotationID) {
                  console.log('entrato ' + annotationID);
                  var comments = script[0].comments;
                  comments.splice(k, 1);
                }
              }
              $('script[type="application/ld+json"]').eq(position).html(JSON.stringify(script, null, 4) + '\n');
              writedArticleContent = $.html();
              fs.writeFile(articleUrl, writedArticleContent, function(err, data) {
                if (err) return res.status(500).send(err);

                res.send({
                  'message': 'Commento inserito con successo!'
                });
                release();
              });
            }
          }
        }
        i++;
      });
    }); //readfile
  }); //mutex
});

apiRoutes.put('/annotations', function(req, res) {
  console.log("REQ: " + req.body);
  console.log(req.body['@id']);
  console.log(req.body.uri);
  var articleUrl = __dirname + '/data/articles/' + req.body.uri;
  var annotationID = req.body['@id'];
  var newText = req.body.text;
  var split = annotationID.split('-');
  var reviewID = split[0];
  mutex.writeLock(function(release) {
    fs.readFile(articleUrl, 'utf8', function(err, articleContent) {
      $ = cheerio.load(articleContent);
      var i = 0;
      // cerco la review
      $('script[type="application/ld+json"]').each(function() {
        var script = JSON.parse($(this).html());
        if (script[0]['@id'] === reviewID) {
          position = i;
          // cerco il commento
          for (var j = 1; j < script.length; j++) {
            if (script[j]['@id'] == annotationID) {
              script[j].text = newText;
              //override dello script
              $('script[type="application/ld+json"]').eq(position).html(JSON.stringify(script, null, 4) + '\n');
              writedArticleContent = $.html();
              fs.writeFile(articleUrl, writedArticleContent, function(err, data) {
                if (err) return res.status(500).send(err);

                res.send({
                  'message': 'Commento modificato con successo!'
                });
                release();
              });
            }
          }
        }
        i++;
      });
    }); //readfile
  }); //mutex*/
});


apiRoutes.post('/review', function(req, res) {
  console.log('Post Annot: ' + JSON.stringify(req.body));
  console.log('Post uri: ' + req.body.uri);
  console.log('Token: ' + req.headers['x-access-token']);
  var articleUrl = __dirname + '/data/articles/' + req.body.uri;
  var annotationBody = req.body;
  var text = req.body.comment;
  var rate = req.body.judgement;
  var role = req.body.userRole;
  // KEY chair please bind
  var type = role == 'chair' ? 'decision' : 'review';
  var status = req.body.accepted ? 'pso:accepted_for_publication' : 'pso:rejected_for_publication';
  console.log(status);
  mutex.writeLock(function(release) {

    fs.readFile(articleUrl, 'utf8', function(err, articleContent) {

      if (err)
        return res.status(404).send({
          'message': 'Errore, articolo non trovato!'
        });
      var position = -1,
        i = 0,
        user = jwt.decode(req.headers['x-access-token']);
      $ = cheerio.load(articleContent);

      $('script[type="application/ld+json"]').each(function() {

        var script = JSON.parse($(this).html());

        if (script[script.length - 1]['@id'] === 'mailto:' + user.email)
          position = i;

        i++;
      });

      //controllo se esite già uno script dell'utente
      if (position >= 0) {
        //seleziono lo script
        var selectedScript = JSON.parse($('script[type="application/ld+json"]').eq(position).html());

        //Pusho il commento nell'array dei comments
        selectedScript[0].article.eval.status = status;
        selectedScript[0].article.eval.rate.value = rate;
        selectedScript[0].article.eval.text = text;

        //override dello script
        $('script[type="application/ld+json"]').eq(position).html(JSON.stringify(selectedScript, null, 4) + '\n');
      }
      //se non esiste creo lo script nel modo corretto
      else {

        var script = $('<script type="application/ld+json"></script>');
        var tmpScript = [];
        var type = role == 'chair' ? 'decision' : 'review';
        console.log('type: ' + type + " role: " + role);
        tmpScript[0] = manipulator.createHeaderWithoutComments(
          type,
          'mailto:' + user.email,
          i + 1,
          new Date(),
          req.body.uri,
          status,
          rate,
          text
        );

        tmpScript[1] = manipulator.createPerson(
          role,
          position,
          'mailto:' + user.email,
          user.given_name + ' ' + user.family_name,
          req.body.uri
        );

        script.html(JSON.stringify(tmpScript, null, 4));
        $('head').append(script + '\n');
      }

      writedArticleContent = $.html();

      fs.writeFile(articleUrl, writedArticleContent, function(err, data) {

        if (err) return res.status(500).send(err);

        res.send({
          'message': 'Commento inserito con successo!'
        });
        release();
      });
    });
  });
});

/*
 * decodifico il token dell'utente, prendo la lista di tutti gli eventi
 * in cui l'utente corrente è pc_member e restituisco il json
 */
apiRoutes.get('/events', function(req, res) {

  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  //salvo il payload
  var user = jwt.decode(token);

  // IMPORTANTE !!!! IN QUESTO CASO UTILIZZO _ID PER FARE RICHIESTE SUCCESSIVE
  DB.collection('events').find({
    $or: [{
      pc_members: user._id
    }, {
      chairs: user._id
    }]
  }).toArray(function(err, docs) {

    if (err) return console.log(err);

    if (docs.length === 0) {

      return res.json({
        success: false,
        message: 'Non ci sono eventi in cui sei pc_member',
        user: user._id //TODO eliminare in fase di produzione (?)
      });

    } else {
      return res.json(docs);
    }
  });
});

app.get('/img/:name', function(req, res) {

  var url = __dirname + '/public/assets/img/' + req.params.name;

  res.sendFile(url);
});

app.get('/yo', function(req, res) {

  getRole('Alice Liddell <alice.liddell@whiterabbit.com>', 'wade-savesd2016.html', function(err, result) {

    res.send(result);
  });
});
/*
 * ogni url per accedere ad una risorsa è condificato con un /api davanti
 *
 */


/*
 * Funzione che controlla se l'utente è il chair della conferenza selezionata in base
 * All'articolo, nel caso in cui non ci sia nessun evento con quel chair e quell'articolo
 * Si cerca ulteriormente all'interno del documento
 */
function getRole(user_id, url, callback) {

  var cond = {
      'submissions.url': url,
      'chairs': user_id
    },
    toFind;
  DB.collection(KEYS.conferences).findOne(cond, function(err, doc) {

    if (err) callback(err, "errore");

    if (!doc) {

      cond = {
        'submissions.url': url,
        'submissions.reviewers': user_id
      };
      toFind = {
        "_id": false,
        "submissions.$": true
      };
      DB.collection(KEYS.conferences).findOne(cond, toFind, function(err, doc) {

        if (err) callback(err, "errore");

        var role = 'pc_member',
          reviewers = doc.submissions[0].reviewers;

        for (var x = 0; x < reviewers.length; x++) {

          if (reviewers[x] === user_id)
            role = 'reviewer';
        }
        callback(null, role);
      });

    } else {

      callback(null, 'chair');
    }
  });
}

app.use('/api', apiRoutes);

/*
 * mette il server in ascolto sulla porta 3000
 *
 */
app.listen(port, function() {
  console.log('easyRash project is listening on http://' + url);
});
