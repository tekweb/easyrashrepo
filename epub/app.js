
var xsltproc = require('xsltproc');

var xslt = xsltproc.transform('data/stylesheet.xsl', 'data/data.xml', {
  "profile": true,
  "output": "data/test.html"
});

xslt.stdout.on('data', function (data) {
  console.log('xsltproc stdout: ' + data);
});

xslt.stderr.on('data', function (data) {
  console.log('xsltproc stderr: ' + data);
});

xslt.on('exit', function (code) {
  console.log('xsltproc process exited with code ' + code);
});
